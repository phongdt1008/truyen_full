package com.example.truyenfull.selector;

public interface ChapterContentBaseSelector {

    String title();

    String content();
}
