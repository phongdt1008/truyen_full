package com.example.truyenfull.selector;

public class TruyenFullStorySelector implements StoryContentBaseSelector<TruyenFullChapterSelector> {

    private TruyenFullStorySelector() {

    }

    private static class SingletonHelper {

        private static final TruyenFullStorySelector INSTANCE = new TruyenFullStorySelector();
    }

    public static TruyenFullStorySelector getInstance() {
        return SingletonHelper.INSTANCE;
    }

    @Override
    public String title() {
        return "h3[class=title][itemprop=name]";
    }

    @Override
    public String author() {
        return "a[itemprop=author]";
    }

    @Override
    public String category() {
        return ".info a[itemprop=genre]";
    }

    @Override
    public String description() {
        return ".desc-text";
    }

    @Override
    public String image() {
        return "img[itemprop=image]";
    }

    @Override
    public String doneFlag() {
        return ".info .text-success";
    }

    @Override
    public String rate() {
        return ".rate-holder";
    }

    @Override
    public String dataFrom() {
        return ".info .source";
    }

    @Override
    public String getChapterList() {
        return ".list-chapter li a";
    }

    @Override
    public TruyenFullChapterSelector getChapterSelector() {
        return TruyenFullChapterSelector.getInstance();
    }

    @Override
    public String getNextChapterPageSelector() {
        return ".pagination li[class=active] + li > a";
    }

    @Override
    public String getCurrChapterPageSelector() {
        return ".pagination li[class=active]";
    }
}
